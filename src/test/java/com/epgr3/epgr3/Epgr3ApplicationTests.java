package com.epgr3.epgr3;

import com.epgr3.epgr3.controller.RecipeController;
import com.epgr3.epgr3.model.Recipe;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

@Transactional
@SpringBootTest
class Epgr3ApplicationTests {

	@Autowired
	private RecipeController recipeController;

	@Test
	public  void TestGetEndpointForRecipe()
	{
		Recipe recipe = new Recipe((long) 1, "Second CHEESECAKE",
				"changed For a recipe with so few ingredients (cream cheese, eggs, sugar, sour cream, vanilla & salt)",
				"second,mMOOZZZZAAAAA eggs, sugar, sour cream, vanilla & salt", "https://i2.wp.com/www.sugarspunrun.com/wp-content/uploads/2019/01/Best-Cheesecake-Recipe-2-1-of-1-2.jpg"
				);
		Recipe returnedRecipe = recipeController.getRecipeById((long)1);

		Assertions.assertEquals(recipe.getContent(), returnedRecipe.getContent());
	}

	@Test
	public  void TestUpdateEndpointForRecipe()
	{
		Recipe recipe = new Recipe((long) 1, "First CHEESECAKE",
				"changed For a recipe with so few ingredients (cream cheese, eggs, sugar, sour cream, vanilla & salt)",
				"second,mMOOZZZZAAAAA eggs, sugar, sour cream, vanilla & salt", "https://i2.wp.com/www.sugarspunrun.com/wp-content/uploads/2019/01/Best-Cheesecake-Recipe-2-1-of-1-2.jpg"
		);
		Recipe returnedRecipe = recipeController.getRecipeById((long)1);
		recipeController.updateRecipeById((long)1, recipe);
		Recipe returnedUpdatedRecipe = recipeController.getRecipeById((long)1);
		Assertions.assertEquals(returnedRecipe.getTitle(), returnedUpdatedRecipe.getTitle());
	}
}
