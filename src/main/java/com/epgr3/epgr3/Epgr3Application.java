package com.epgr3.epgr3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

//@EntityScan("com.epgr3.epgr3.model.User,com.epgr3.epgr3.model.Post")

@SpringBootApplication
public class Epgr3Application {

	public static void main(String[] args) {
		SpringApplication.run(Epgr3Application.class, args);
	}

}
