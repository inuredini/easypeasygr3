package com.epgr3.epgr3.repo;

import com.epgr3.epgr3.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    @Query("SELECT u from User  u where u.email = :email AND u.password = :password")
    User  signIn(@Param("email") String email,@Param("password")  String password);
}
