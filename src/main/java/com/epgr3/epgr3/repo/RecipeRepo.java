package com.epgr3.epgr3.repo;

import com.epgr3.epgr3.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import javax.transaction.Transactional;

@Transactional
public interface RecipeRepo extends JpaRepository<Recipe, Long> {
}
