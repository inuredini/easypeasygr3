package com.epgr3.epgr3.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name="recipe")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Recipe {
    @Id
    @Column(name="id",nullable = false)
    @SequenceGenerator(name="recipe_seq",sequenceName = "recipe_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "recipe_seq")
    private Long id;

    @Column(length=1024)
    private String title;

    @Column(length=10240)
    private String content;

    @Column(length=10240)
    private String ingredients;

    @Column(length=10240)
    private String imageLink;


    public Recipe(Long id, String title, String content, String ingredients, String imageLink) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.ingredients = ingredients;
        this.imageLink = imageLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
