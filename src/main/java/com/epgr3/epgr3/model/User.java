package com.epgr3.epgr3.model;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name="users")
@NoArgsConstructor
@Data
@DynamicUpdate
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id",nullable = false)
    private long id;

    //@NotBlank(message = " Full Name is mandatory")
    @Column(length=1024)
    private String name;

    //@NotBlank(message = "Email is mandatory")
    @Column(length=1024)
    private String email;

    //@NotBlank(message = "Password is mandatory")
    @Column(length=1024)
    private String password;

    public User(long id, String name, String email, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
// standard constructors / setters / getters / toString
}

