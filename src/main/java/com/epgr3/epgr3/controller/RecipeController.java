package com.epgr3.epgr3.controller;

import com.epgr3.epgr3.exception.PostNotFound;
import com.epgr3.epgr3.model.Recipe;
import com.epgr3.epgr3.repo.RecipeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecipeController {

    @Autowired
    private RecipeRepo recipeRepo;

    /**
     * Returns an Recipe object that will be returned as json format after it have been created.
     * The endpoint is called using POST request on {server_ip}/recipe with body
     * @param  recipe  an recipe object
     * @return      the recent created recipe object
     * @see         Recipe
     */
    @PostMapping("/recipe")
    public Recipe createRecipe(@RequestBody Recipe recipe){
        Recipe result = recipeRepo.save(recipe);
        return result;
    }

    /**
     * Returns all Recipe object as pageable.
     * The endpoint is called using GET request on {server_ip}/recipe
     * @param  pageable  an pageable object
     * @return      all created recipe object
     * @see         Page<Recipe>
     */
    @GetMapping("/recipe")
    public Page<Recipe> listRecipe(Pageable pageable){
        // Get all receipes
        return recipeRepo.findAll(pageable);
    }

    /**
     * Returns an Recipe object for the given id.
     * The endpoint is called using GET request on {server_ip}/recipe/{id}
     * @param  id  an recipe id
     * @return      the recent created recipe object
     * @see         Recipe
     */
    @GetMapping("/recipe/{id}")
    public Recipe getRecipeById(@PathVariable Long id){
        // Get receipe for the given id
        Recipe recipe = recipeRepo.getOne(id);
        return recipe;
    }

    /**
     * Returns a Response message (for successful returs "ok").
     * The endpoint is called using PUT request on {server_ip}/recipe/{id} with body
     * @param  id  an recipe id
     * @param recipe  the object which hold the values you want to update
     * @return      response message
     * @see         ResponseEntity<Recipe>
     */
    @PutMapping("/recipe/{id}")
    public ResponseEntity<Recipe>  updateRecipeById(@PathVariable(value = "id") Long id, @RequestBody Recipe recipe)
    {
        Recipe a_recipe = recipeRepo.getOne(id);
        a_recipe.setTitle(recipe.getTitle());
        a_recipe.setContent(recipe.getContent());
        a_recipe.setImageLink(recipe.getImageLink());
        a_recipe.setIngredients(recipe.getIngredients());
        final Recipe updatedRecipe = recipeRepo.save(recipe);
        return ResponseEntity.ok(updatedRecipe);
    }

    /**
     * Returns a Response message (for successful returs "ok" or error message).
     * The endpoint is called using DELETE request on {server_ip}/recipe/{id}
     * @param  id  an recipe id
     * @return      response message
     * @see         ResponseEntity<?>
     */
    @DeleteMapping("/recipe/{id}")
    public ResponseEntity<?> deleteRecipe(@PathVariable Long id){
        // Delete receipe for the given id
        return recipeRepo.findById(id)
                .map(post -> {
                    recipeRepo.delete(post);
                    return ResponseEntity.ok().build();
                }).orElseThrow(()-> new PostNotFound("Recipe with the given id: "+ id.toString() + " do NOT EXIST"));
    }
}
