package com.epgr3.epgr3.controller;

import com.epgr3.epgr3.model.User;
import com.epgr3.epgr3.repo.RecipeRepo;
import com.epgr3.epgr3.repo.UserRepository;
import org.apache.catalina.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @Value("${spring.application.name}")
    String appName;

    @Autowired
    private UserRepository userRepo;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "index";
    }

    @GetMapping("/register")
    public String showSignUpForm(Model model) {

        model.addAttribute("user", new User(0,"","","" ));
        return "signup";
    }

    @PostMapping("/adduser")
    public String addUser( User user, Model model) {

        userRepo.save(user);
        model.addAttribute("users", userRepo.findAll());
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String displayLogin( User user, Model model) {
        model.addAttribute("user", new User(0,"","","" ));
        return "login";
    }

    @PostMapping("/login")
    public String signin( User user, Model model) {

        User existingUser = userRepo.signIn( user.getEmail(), user.getPassword());
                //.findById(id)
               // .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        System.out.println(existingUser.getEmail());
        if (existingUser != null) {
            return "redirect:/main";
        }
        return "signup";
    }

    @GetMapping("/main")
    public String displayMain( User user, Model model) {
        model.addAttribute("user", new User(0,"","","" ));
        return "post-list";
    }
}
